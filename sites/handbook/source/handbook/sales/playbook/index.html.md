---
layout: handbook-page-toc
title: "Enterprise Sales Playbook"
description: "The GitLab Enterprise Sales Playbook offers actionable and prescriptive guidance throughout the customer lifecycle to drive repeatable and consistent sales performance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Overview 
The GitLab Enterprise Sales Playbook offers actionable and prescriptive guidance throughout the customer lifecycle to drive repeatable and consistent sales performance. 
- [Lessons from a Sales Veteran](https://audiblereadypodcast.libsyn.com/41-lessons-from-a-sales-veteran-w-frank-azzolino) (podcast, 37 minutes)
- [Most Frequently Asked Questions from Salespeople](https://audiblereadypodcast.libsyn.com/27-our-most-faqs-from-salespeople-w-john-kaplan) (podcast, 14 minutes)

Note: A Commercial Sales Playbook will be developed as a component of the [Commercial Sales Handbook](/handbook/sales/commercial/) (timing TBD).

## Educate & Engage

### [Prospecting](/handbook/sales/prospecting/)

## Facilitate the Opportunity

### [Discovery](/handbook/sales/playbook/discovery/)

### [Command Plan](/handbook/sales/command-of-the-message/command-plan/)

### [Proof of Value (POV)](/handbook/customer-success/solutions-architects/tools-and-resources/pov/)

#### [Demonstrations](/handbook/customer-success/solutions-architects/demonstrations/)
- [The Art of the Demo](https://audiblereadypodcast.libsyn.com/the-art-of-the-demo-w-john-kaplan) podcast (19 minutes)

### Making the Business Case
- [Demystifying the Metrics Conversation](/handbook/sales/command-of-the-message/metrics/)
- [Competing Against Doing Nothing](https://audiblereadypodcast.libsyn.com/35-competing-against-do-nothing-w-john-kaplan) (podcast, 8.5 minutes)

### [Champion Development](/handbook/sales/meddppicc/#champion) 

## Deal Closure

### [Mutual Close Plan](/handbook/sales/mutual-close-plan) 

### Create a Customer-Centric Proposal

### [Negotiate to Close](/handbook/sales/negotiate-to-close/)

### [Order Processing](/handbook/sales/field-operations/order-processing/)
- [Quote Configuration](/handbook/sales/field-operations/order-processing/#quote-configuration)
- [How to Send an Order Form to a Customer](/handbook/sales/field-operations/order-processing/#how-to-send-an-order-form-to-a-customer)
- [Submit an Opportunity for Booking](/handbook/sales/field-operations/order-processing/#submit-an-opportunity-for-booking)

## Retain and Expand

### [Smooth Account Transition](/handbook/customer-success/pre-sales-post-sales-transition/)

### [Customer Onboarding](/handbook/customer-success/tam/onboarding/)

### [Success Plans](/handbook/customer-success/tam/success-plans/)

### [Account Planning](/handbook/sales/account-planning/)

### [Executive Business Reviews](/handbook/customer-success/tam/ebr/)

### [Customer Health Assessment and Management](/handbook/customer-success/tam/health-score-triage/)

### [Renewals](/handbook/customer-success/tam/renewals/)
