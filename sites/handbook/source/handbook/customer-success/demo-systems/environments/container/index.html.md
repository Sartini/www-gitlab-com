---
layout: handbook-page-toc
title: "Container Sandbox"
description: "The GitLab Demo Systems container environment handbook pages provides an overview of how our cluster infrastructure is configured and has answers to frequently asked questions."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Google Kubernetes Engine (GKE) Environment

[Learn more about the GKE environment](/handbook/customer-success/demo-systems/environments/container/gke/)

## Amazon Web Services (AWS) Elastic Kubernetes Service (EKS) Environment

[Learn more about the EKS environment](/handbook/customer-success/demo-systems/environments/container/eks/)
